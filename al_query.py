# -*- coding: utf-8 -*-
"""
Created on Sun Jan  6 02:26:25 2019

@author: Administrator
"""

import random
from data_box import sql_read


def simulation(pool_type):
    """
    # 模拟建造
    # 参数(标*为必填参数)：
    # pool_type   奖池类型*
    # 返回:
    # 返回一个包含元组的列表
    """
    point = random.random()
    if point <= 0.07:
        quality = "ssr"
    elif point <= 0.19:
        quality = "sr"
    elif point <= 0.45:
        quality = "r"
    else:
        quality = "n"
    DRL = "{}' and quality = '{}".format(pool_type+1, quality)
    result = sql_read("User.db", 'Simulation_prize_pool',
                      'prize_pool', DRL, "name")
    result = random.choice(result)[0]
    return result, quality
