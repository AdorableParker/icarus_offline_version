"""
# 爬取B站
"""
import json
import time
import requests
import reto


def get_top():
    """
    # 爬取B站排行榜
    """

    params = (
            ('order', 'all'),
    )

    response = requests.get('https://app.bilibili.com/x/v2/rank',
                            params=params, verify=False)
    response.encoding = "UTF-8"
    content = response.text
    text = json.loads(content)["data"]
    echo = []
    for out in text[:3]:
        echo.append({
            "AV": out['param'],
            "播放量": out['play'],
            "评分": out['pts'],
            "标题": out["title"]
         })

    return echo


def get_upper(oid):
    """
    # 更新B站官方动态
    """
    params = (('oid', oid), ('type', "11"))
    response = requests.get('https://api.bilibili.com/x/v2/reply/cursor',
                            params=params, verify=False)
    response.encoding = "UTF-8"
    content = response.text
    text = json.loads(content)
    text = text["data"]["top"]["upper"]["content"]["message"]
    return text


def get_trend(uid, flug=True):
    """
    # 爬取B站动态
    """
    url = 'http://api.vc.bilibili.com/dynamic_svr/v1/dynamic_svr/space_history'
    response = requests.get(url, params=(('host_uid', uid),))
    response.encoding = "UTF-8"
    content = json.loads(response.text)["data"]["cards"][0]
    uname = content["desc"]['user_profile']["info"]['uname']
    if flug:
        text_time = time.strftime('%Y-%m-%d %H:%M:%S',
                                  time.localtime(content["desc"]["timestamp"]))
    else:
        text_time = content["desc"]["timestamp"]
    text = json.loads(content["card"])
    if "item" in text:
        text = text["item"]
        if "description" in text:
            text1 = text["description"]
            img_src = text["pictures"]
            img = ""
        else:
            text1 = text["content"]
            img_src = [{"img_src": ""}]
            img = ""

        for img_uil in img_src:
            img = img + img_uil["img_src"] + "\n"

        if reto.reto(text1, ["评论接~", "见评论", "见置顶", "置顶"]):
            oid = content['desc']['rid']
            text1 = text1 + get_upper(oid)

    else:
        text1, img = "专栏标题" + text["title"], ""

    return (text1, img, text_time, uname)


if __name__ == '__main__':
    AX = get_top()
    AXX = get_trend('233114659')
    print(AXX)
