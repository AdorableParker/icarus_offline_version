# -*- coding: utf-8 -*-
# Python code generated with wxFormBuilder(version Jun 17 2015)
# http://www.wxformbuilder.org/

import time
import wx
import wx.xrc
import requests

from pathlib import Path
import al_producetime_query as apq
import al_query
import data_box
import reto
import update_operation

from code_conversion import code_name
from crawler import get_trend as get_trend


class mainWindow(wx.Frame):
    """
    # 主要类
    """
    def __init__(self, parent, updatainfo, version_number):
        # 实例化窗口
        wx.Frame.__init__(
                self, parent, id=wx.ID_ANY,
                title="伊卡洛斯 Beta 当前数据库版本:" + version_number,
                pos=wx.DefaultPosition,
                size=wx.Size(484, 435),
                style=wx.CAPTION | wx.CLOSE_BOX |
                wx.MINIMIZE_BOX | wx.TAB_TRAVERSAL)
        self.icon = wx.Icon(name="logo.ico", type=wx.BITMAP_TYPE_ICO)
        self.SetIcon(self.icon)
        # 配置定位器
        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize, wx.DefaultSize)

        # 实例化菜单栏
        filemenu = wx.Menu()
        menuAbout = filemenu.Append(wx.ID_ABOUT, "关于", "关于此程序")
        menuExit = filemenu.Append(wx.ID_EXIT, "退出", "退出此程序")
        menuBar = wx.MenuBar()
        menuBar.Append(filemenu, "文件")

        # 实例化框架
        bSizer_A = wx.BoxSizer(wx.VERTICAL)
        bSizer_AA = wx.StaticBoxSizer(wx.StaticBox(
                self, wx.ID_ANY, "记得填参数"), wx.VERTICAL)
        bSizer_AAA = wx.BoxSizer(wx.HORIZONTAL)
        bSizer_AB = wx.StaticBoxSizer(
                wx.StaticBox(self, wx.ID_ANY, "建造模拟器"), wx.HORIZONTAL)
        bSizer_ABB = wx.BoxSizer(wx.VERTICAL)
        bSizer_ABA = wx.BoxSizer(wx.VERTICAL)
        bSizer_ABAA = wx.BoxSizer(wx.HORIZONTAL)
        bSizer_ABAB = wx.BoxSizer(wx.HORIZONTAL)
        # 实例化标签
        self.input_label = wx.StaticText(
                bSizer_AA.GetStaticBox(),
                wx.ID_ANY, "参数：",
                wx.DefaultPosition,
                wx.DefaultSize, 0)
        description = '''由于模拟建造程序设计为阻塞方式运行,所以会呈现出像是卡死的状态,属正常情况,请不要反复点击,这是我用来学习Python GUI的一个练习,如果有bug，请多多包涵-------萌萌哒帕克\n伊卡洛斯工具箱-Beta版本'''
        self.description_label = wx.StaticText(bSizer_AB.GetStaticBox(),
                                               wx.ID_ANY, description,
                                               wx.DefaultPosition,
                                               wx.DefaultSize, 0)
        self.count_label = wx.StaticText(bSizer_AB.GetStaticBox(),
                                         wx.ID_ANY, "建造次数：",
                                         wx.DefaultPosition,
                                         wx.DefaultSize, 0)
        self.frequency_label = wx.StaticText(bSizer_AB.GetStaticBox(),
                                             wx.ID_ANY, "建造时间[ms/次]：",
                                             wx.DefaultPosition,
                                             wx.DefaultSize, 0)

        # 实例化按钮
        bSizer_AAB = wx.BoxSizer(wx.HORIZONTAL)
        self.update_button = wx.Button(bSizer_AA.GetStaticBox(),
                                       wx.ID_ANY, "查询小加加B博动态",
                                       wx.DefaultPosition,
                                       wx.DefaultSize, 0)
        self.time_button = wx.Button(bSizer_AA.GetStaticBox(),
                                     wx.ID_ANY, "查询建造时间",
                                     wx.DefaultPosition,
                                     wx.DefaultSize, 0)
        self.sakura_button = wx.Button(bSizer_AA.GetStaticBox(),
                                       wx.ID_ANY, "重樱船名对照",
                                       wx.DefaultPosition,
                                       wx.DefaultSize, 0)
        self.query_button = wx.Button(bSizer_AB.GetStaticBox(),
                                      wx.ID_ANY, "开始建造",
                                      wx.DefaultPosition, wx.DefaultSize, 0)

        # 实例化文本框
        self.parameter_text_box = wx.TextCtrl(bSizer_AA.GetStaticBox(),
                                              wx.ID_ANY, wx.EmptyString,
                                              wx.Point(-1, -1),
                                              wx.Size(-1, -1),
                                              wx.TE_MULTILINE)

        #  实例化滑轮框
        self.parameter_input_box = wx.SpinCtrl(bSizer_AB.GetStaticBox(),
                                               wx.ID_ANY, "1",
                                               wx.DefaultPosition,
                                               wx.DefaultSize,
                                               wx.SP_ARROW_KEYS,
                                               1, 1000, 0)
        self.frequency_input_box = wx.SpinCtrl(bSizer_AB.GetStaticBox(),
                                               wx.ID_ANY, "1",
                                               wx.DefaultPosition,
                                               wx.DefaultSize,
                                               wx.SP_ARROW_KEYS,
                                               1, 1000, 0)
        # 实例化选择框
        self.selection_box = wx.RadioBox(bSizer_AB.GetStaticBox(),
                                         wx.ID_ANY, "选择池子",
                                         wx.DefaultPosition, wx.DefaultSize,
                                         ["轻型建造", "重型建造", "特型建造"],
                                         1, wx.RA_SPECIFY_COLS)

        # 配置选择框属性
        self.selection_box.SetSelection(0)

        # 配置标签属性
        self.input_label.Wrap(-1)
        self.description_label.Wrap(-1)
        self.count_label.Wrap(-1)
        self.frequency_label.Wrap(-1)
        self.input_label.SetFont(wx.Font(12, 70, 90, 92, False, "等线"))
        # 配置文本框属性
        self.parameter_text_box.SetFont(wx.Font(10, 70, 90, 92, False, "等线"))

        # 由上至下 深度优先 添加组件
        # A
        # ║
        # ╠════AA════╦═════AAA════╦════input_label
        # ║          ║            ║
        # ║          ║            ╚════parameter_text_box
        # ║          ║
        # ║          ╚═════AAB════╦════update_button
        # ║                       ║
        # ║                       ╠════time_button
        # ║                       ║
        # ║                       ╚════sakura_button
        # ║
        # ║          ╔═════ABA════╦════ABAA════╦════count_label
        # ║          ║            ║            ║
        # ║          ║            ║            ╚════parameter_input_box
        # ║          ║            ║
        # ║          ║            ╠════ABAB════╦════frequency_label
        # ║          ║            ║            ║
        # ║          ║            ║            ╚════frequency_input_box
        # ║          ║            ║
        # ║          ║            ╚════selection_box
        # ║          ║
        # ║          ║            ╔════description_label
        # ║          ║            ║
        # ╚════AB════╩═════ABB════╩════query_button
        bSizer_A.Add(bSizer_AA, 1,
                     wx.EXPAND | wx.ALIGN_CENTER_HORIZONTAL | wx.ALL, 5)

        bSizer_AA.Add(bSizer_AAA, 1,
                      wx.EXPAND | wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        bSizer_AAA.Add(self.input_label, 0,
                       wx.ALL | wx.EXPAND | wx.ALIGN_CENTER_VERTICAL, 5)
        bSizer_AAA.Add(self.parameter_text_box, 1,
                       wx.EXPAND | wx.ALIGN_CENTER_VERTICAL, 5)

        bSizer_AA.Add(bSizer_AAB, 1,
                      wx.ALL | wx.ALIGN_CENTER_HORIZONTAL | wx.EXPAND, 0)

        bSizer_AAB.Add(self.update_button, 1,
                       wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        bSizer_AAB.Add(self.time_button, 1,
                       wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        bSizer_AAB.Add(self.sakura_button, 1,
                       wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        bSizer_A.Add(bSizer_AB, 1,
                     wx.EXPAND | wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        bSizer_AB.Add(bSizer_ABA, 1, wx.EXPAND | wx.ALIGN_CENTER_VERTICAL, 5)

        bSizer_ABA.Add(bSizer_ABAA, 1,
                       wx.ALL | wx.EXPAND | wx.ALIGN_CENTER_VERTICAL, 5)

        # 次数标签
        bSizer_ABAA.Add(self.count_label, 1, wx.ALIGN_CENTER_VERTICAL, 5)
        # 次数编辑框
        bSizer_ABAA.Add(self.parameter_input_box, 1,
                        wx.ALIGN_CENTER_VERTICAL, 5)

        bSizer_ABA.Add(bSizer_ABAB, 1,
                       wx.ALL | wx.EXPAND | wx.ALIGN_CENTER_VERTICAL, 5)

        # 频率标签
        bSizer_ABAB.Add(self.frequency_label, 1, wx.ALIGN_CENTER_VERTICAL, 5)
        # 次数编辑框
        bSizer_ABAB.Add(self.frequency_input_box, 1,
                        wx.ALIGN_CENTER_VERTICAL, 5)

        bSizer_ABA.Add(self.selection_box, 4, wx.ALL | wx.EXPAND, 0)

        bSizer_AB.Add(bSizer_ABB, 1,
                      wx.ALL | wx.EXPAND | wx.ALIGN_CENTER_VERTICAL, 5)

        bSizer_ABB.Add(self.description_label, 5, 0, 5)
        bSizer_ABB.Add(self.query_button,
                       1, wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.EXPAND, 5)

        # 将框架放入窗口
        self.SetSizer(bSizer_A)
        self.Layout()
        self.Centre(wx.BOTH)
        self.SetMenuBar(menuBar)
        self.CreateStatusBar()
        # Connect Events
        self.update_button.Bind(wx.EVT_BUTTON, self.update_crawler)
        self.time_button.Bind(wx.EVT_BUTTON, self.query_time)
        self.sakura_button.Bind(wx.EVT_BUTTON, self.sakura_name)
        self.query_button.Bind(wx.EVT_BUTTON, self.query)
        self.Bind(wx.EVT_MENU, self.OnAbout, menuAbout)
        self.Bind(wx.EVT_MENU, self.OnExit, menuExit)

        self.Show(True)

        # 更新结果反馈
        dlg = wx.MessageDialog(self, updatainfo, "数据库更新情况", wx.OK)
        dlg.ShowModal()
        dlg.Destroy()
        return

    def update_crawler(self, event):
        """
        # 小加加更新
        """
        text = get_trend('233114659')
        text = "更新时间:{0[2]}\n\n{0[0]}".format(text)
        infowindow(None, True, text)
        return

    def query_time(self, event):
        """
        # 建造时间查询
        """
        input_text = self.parameter_text_box.GetValue()
        if not input_text:
            output = "你还没填写要查的船名或者时间呢"
        else:
            input_text = input_text.replace('：', ':')
            time_key = reto.r2n(input_text, [r"\d\:\d\d"])
            if time_key == "Public":
                input_text = input_text.upper()
                output = apq.al_query_time(input_text)
            else:
                output = apq.al_query_name(time_key)
        dlg = wx.MessageDialog(self, output, "建造时间查询结果", wx.OK)
        dlg.ShowModal()
        dlg.Destroy()
        return

    def sakura_name(self, event):
        """
        # 重樱船名查询
        """
        input_text = self.parameter_text_box.GetValue()
        if not input_text:
            echo = "你还没填写要查的船名呢"
        else:
            output = code_name(input_text)
            if output[0]:
                echo = output[1]
            else:
                echo = "船名 {} 有误或未收录".format(input_text)
        dlg = wx.MessageDialog(self, echo, "重樱船名查询结果", wx.OK)
        dlg.ShowModal()
        dlg.Destroy()
        return

    def query(self, event):
        pool_type = self.selection_box.GetSelection()
        number = self.parameter_input_box.GetValue()
        frequency = self.frequency_input_box.GetValue()
        frequency = int(frequency)/1000
        output_dict = {}
        for i in range(number):
            output, quality = al_query.simulation(pool_type)
            if not output_dict.get(quality):
                output_dict[quality] = {output: 0}
            if not output_dict[quality].get(output):
                output_dict[quality][output] = 0
            output_dict[quality][output] += 1
            time.sleep(frequency)
        output = "建造报告：\n\n共建出\r"
        for x in output_dict:
            listsum = sum(output_dict[x].values())
            output += '{}级:{}艘,分别为'.format(x, listsum)
            for y in output_dict[x]:
                output += "\r{}:{}艘".format(y, output_dict[x][y])
            output += "\n"
        dlg = wx.MessageDialog(self, output, "模拟建造结果", wx.OK)
        dlg.ShowModal()
        dlg.Destroy()
        return

    def OnAbout(self, event):
        text = """本软件由wxpython，Python编写\nGitLab项目地址：\r
        https://gitlab.com/AdorableParker/icarus_offline_version.git"""
        infowindow(None, False, text)
        return

    def OnExit(self, event):
        """
        # '退出'按钮的响应函数
        """
        self.Close(True)
        return


class infowindow(wx.Frame):
    """
    # 消息窗口
    """
    def __init__(self, parent, img, text):
        # 实例化窗口框架
        wx.Frame.__init__(self, parent, id=wx.ID_ANY,
                          title=" ฅ՞•ﻌ•՞ฅ ",
                          pos=wx.DefaultPosition,
                          size=wx.Size(500, 300),
                          style=wx.CAPTION | wx.CLOSE_BOX |
                          wx.TAB_TRAVERSAL | wx.RESIZE_BORDER)
        self.icon = wx.Icon(name="logo.ico", type=wx.BITMAP_TYPE_ICO)
        self.SetIcon(self.icon)
        # 配置定位器
        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize, wx.DefaultSize)

        # 实例化框架
        bSizer_B = wx.BoxSizer(wx.VERTICAL)
        bSizer_BA = wx.BoxSizer(wx.HORIZONTAL)
        bSizer_BB = wx.BoxSizer(wx.HORIZONTAL)

        # 实例化图片框
        if img:
            img = "img.png"
        else:
            img = "logo.png"
        self.img = wx.StaticBitmap(
                self, wx.ID_ANY,
                wx.Bitmap(img, wx.BITMAP_TYPE_ANY),
                wx.Point(-1, -1), wx.Size(5, 5), 0)

        # 实例化标签
        self.info_label = wx.StaticText(self, wx.ID_ANY, text,
                                        wx.DefaultPosition,
                                        wx.DefaultSize, wx.ALIGN_LEFT)

        # 实例化按钮
        self.enter_buttom = wx.Button(self, wx.ID_ANY, "确认",
                                      wx.DefaultPosition, wx.DefaultSize, 0)
        # 配置按钮属性
        self.enter_buttom.SetMinSize(wx.Size(100, 30))
        self.enter_buttom.SetMaxSize(wx.Size(100, 30))
        # 配置图片框属性
        self.img.SetFont(wx.Font(
                wx.NORMAL_FONT.GetPointSize(),
                70, 90, 90, False, wx.EmptyString))
        self.img.SetMinSize(wx.Size(150, 150))
        self.img.SetMaxSize(wx.Size(150, 150))

        # 配置标签属性
        self.info_label.Wrap(-1)
        self.info_label.SetFont(wx.Font(
                wx.NORMAL_FONT.GetPointSize(),
                70, 90, 90, False, wx.EmptyString))

        # 由上至下 深度优先 添加组件
        # B
        # ║
        # ╠══BA══╦══img
        # ║      ║
        # ║      ╚══info
        # ║
        # ╠═NULL
        # ║
        # ║
        # ╠══BB══╦══NULL
        # ║      ║
        # ║      ╠═buttom
        # ║      ║
        # ║      ╚══NULL
        # ║
        # ╚═NULL

        bSizer_B.Add(bSizer_BA, 20, wx.EXPAND, 5)

        bSizer_BA.Add(self.img, 1,
                      wx.ALL | wx.EXPAND, 5)
        bSizer_BA.Add(self.info_label, 5,
                      wx.ALL | wx.ALIGN_CENTER_HORIZONTAL |
                      wx.ALIGN_CENTER_VERTICAL | wx.EXPAND, 5)

        bSizer_B.Add(0, 5, 0)
        bSizer_B.Add(bSizer_BB, 2, wx.ALIGN_CENTER_HORIZONTAL | wx.EXPAND, 5)

        bSizer_BB.Add(400, 0, 0)
        bSizer_BB.Add(self.enter_buttom, 1, wx.ALL | wx.ALIGN_BOTTOM, 5)
        bSizer_BB.Add(5, 0, 0)

        bSizer_B.Add(0, 5, 0)

        # 将框架放入窗口
        self.SetSizer(bSizer_B)
        self.Layout()
        self.Centre(wx.BOTH)

        # Connect Events
        self.enter_buttom.Bind(wx.EVT_BUTTON, self.OnExit)
        self.Show(True)

    # Virtual event handlers, overide them in your derived class
    def OnExit(self, event):
        """
        # 退出
        """
        self.Close(True)


def updata(version_number):
    """
    # 更新数据库
    # 参数
    # get               是否获取数据库
    # version_number    最新数据库版本
    """
    try:
        url = 'https://gitlab.com/AdorableParker/icarus_offline_version/raw/master/User.db'
        r = requests.get(url)
        code = open("User.db", "wb")
        code.write(r.content)
        code.close()
        data_box.sql_rewrite("User.db", "version_number",
                             "version", " ", "version", version_number)
        return True
    except BaseException:
        return False


def check_version():
    """
    # 检查版本
    """
    try:
        now_version_number = data_box.sql_read('User.db',
                                               'version_number',
                                               in_where=False)[0][0]
        return update_operation.check_for_updates(now_version_number)
    except BaseException:
        return True


pathos = Path("User.db")
if not pathos.is_file() or check_version():
    # 如果数据库文件不存在 或 版本号不符
    try:
        version_number = update_operation.get_version_number()
    except BaseException:
        version_number = "获取数据库版本失败"
        updata(" ")
        updatainfo = version_number
    else:
        if not updata(version_number):
            updatainfo = "更新数据库失败"
        else:
            updatainfo = "更新成功"
else:
    updatainfo = "无需更新数据库"
img = data_box.sql_read('User.db', 'img', in_where=False)
for oi in img:
    file = open(oi[0], mode='wb')
    file.write(oi[1])
    file.close()
# 定义实例对象
app = wx.App(False)
# 实例化主窗口
mainWindow(None, updatainfo, version_number)
# 启动响应循环
app.MainLoop()
