# -*- coding: utf-8 -*-
'''
# 判断更新数据库
'''
import requests
import lxml
from bs4 import BeautifulSoup


def get_version_number():
    """
    # 获取版本号
    """
    response = requests.get('https://gitlab.com/AdorableParker/icarus_offline_version')
    htmltext = BeautifulSoup(response.text, "html.parser")
    dive = htmltext.find("div", "label label-monospace")
    version_number = dive.get_text()
    version_number.replace("\n", "")
    return version_number


def check_for_updates(now_version_number):
    """
    # 检查更新
    # 参数
    # now_version_number 当前版本号
    """
    version_number = get_version_number()
    if version_number != now_version_number:
        return True
    return False
